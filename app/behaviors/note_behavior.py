from ferris.core.ndb import Behavior
from app.behaviors.sanitize import Sanitize


class NoteBehavior(Behavior):
    sanitizer = Sanitize()

    def before_put(self, instance):
        instance.message = self.sanitizer.sanitize_text(instance.message)
