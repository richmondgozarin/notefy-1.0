from ferris import Controller, route, route_with, messages
from google.appengine.api import memcache
from google.appengine.ext import ndb
from app.models.notefire import Notefire
from app.components.notefires import Notefires
from plugins.firebase_token_generator import create_token
from plugins.firebase_rest import FirebaseRest
from ferris.core.json_util import stringify as json_stringify
from ferris.core.template import render_template
from app.components.firebase_behavior import FirebaseActions
import logging
import json
from app.components.utilities import check_json
from app.components.user_auth import allow_access
from ferris.core.template import render_template


class Notefires(Controller):
    class Meta:
            prefixes = ('api',)
            components = (Notefires,messages.Messaging,)
            Model = Notefire

    @route
    def api_canvaslists(self):
        return json_stringify(self.components.notefires.get_canvas_data())

    @route
    def api_userslists(self):
        return json_stringify(self.components.notefires.get_users_data())

    @route
    def api_createuser(self):
        if allow_access:
            return self.components.notefires.signup_process()
        else:
            return 400

    @route
    def api_login(self):
        self.components.notefires.login()
        return render_template("notefires/login.html")

    @route
    def api_home(self):#it process the log in details
        return self.components.notefires.home()

    @route
    def api_save_notes(self):
        return self.components.notefires.save_notes()

    @route
    def api_save_canvas(self):
        return self.components.notefires.save_canvas()



    @route
    def api_signup(self):
        return render_template("notefires/signup.html")

    @route
    def signup_process(self):
        return self.components.notefires.signup_process()

    @route
    def login_process(self):
        return self.components.notefires.login_process()



    @route_with(template="/api/frequencys/activate_mapping/<urlsafe_key>")
    def activate_mapping(self, urlsafe_key):
        # Get first the Active Mapping and delete to Firebase
        current_active_mapping = Notefire.get_active_mapping().get()
        print current_active_mapping
        # FirebaseActions.remove('FREQUENCY', current_active_mapping.key.urlsafe())

        # # Delete All to Datastore
        # Review.deactivate_all()
        # result = Review.activate_frequency_mapping(urlsafe_key)
        # if result:
        #     print self.util.stringify_json(True)
        #     return self.util.stringify_json(True)
        # print self.util.stringify_json(False)
        # return self.util.stringify_json(False)



    @route
    def ewaw(self):
        return self.components.notefires.ewaw()


    @route
    def open_canvas(self):
        return self.components.notefires.open_canvas()

