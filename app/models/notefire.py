from ferris import Model, ndb
from ferris.behaviors import searchable
from app.components.firebase_behavior import FirebaseBehavior




class Notefire(Model):
    class Meta:
        behaviors = (searchable.Searchable, FirebaseBehavior, )
        search_index = ('global',)

    email = ndb.StringProperty()
    password = ndb.StringProperty()

    status = ndb.StringProperty(indexed=True, default="INACTIVE")



    @classmethod
    def create(cls, params):
        item = cls(email = params['email'],
                   password = params['password'])
        item.put()





    @classmethod
    def get_active_mapping(cls):
        print "nandito ako sa get_active_mapping!!!!!!!!!!!"
        return cls.query().filter(cls.status == 'ACTIVE')





    @classmethod
    def list(cls):
        return cls.query().order(cls.created).fetch()



    @classmethod
    def semail(cls, email):
        return cls.find_all_by_email(email).get()




    @classmethod
    def query_key(cls, keyname):
        instance = ndb.Key(cls, keyname).get()
        if instance is not None:
            return instance
        return None


