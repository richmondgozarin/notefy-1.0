from plugins.firebase_token_generator import create_token
import datetime
from plugins.firebase_rest import FirebaseRest


class FIREBASE_API():

    def create_token(self, data=None, is_admin=False):
        today = datetime.datetime.now()
        expiration = today + datetime.timedelta(hours=2)

        if data is None:
            data = {"auth_data": "foo", "other_auth_data": "bar"}

        options = {"expires": expiration}

        if(is_admin):
            options['admin'] = True

        f = FirebaseRest('foo')
        setting = f.get_env_settings()
        firebase_secret = setting['secret']
        return create_token(firebase_secret, data, options)
