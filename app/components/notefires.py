# from google.appengine.ext import ndb
# from plugins.firebase_token_generator import create_token
# from plugins.firebase_rest import FirebaseRest
# from ferris.core.template import render_template
from app.models.notefire import Notefire
from ferris.core.json_util import stringify as json_stringify
from ferris.components.search import Search
import logging
import json
from app.components.firebase_behavior import FirebaseActions
import datetime
import time
from ferris.core.template import render_template

class Notefires(object):



    def __init__(self, controller):
        self.controller = controller
        self.notefire = Notefire()


    def session_isset(self):
        if self.controller.session['youser'] is None:
            return False
        else:
            return self.controller.session['youser']

    def login(self):
        self.controller.session['youser'] = None
        # logging
        # logging.info(self.controller.session['youser'])
        self.controller.session['pw'] = None
        return self.controller.session['youser']

    def sample(self):
        return 200

    def signup_process(self):
        email = self.controller.request.params['email']
        username = self.controller.request.params['username']
        password = self.controller.request.params['password']
        r = FirebaseActions.push("Users",{"email": email, "username": username, "password": password})
        return self.controller.redirect(self.controller.uri(action="login"))

    def save_canvas(self):
        for pars in self.controller.request.params:
            if pars == "txtAreaIdHide" or pars == "personal":
                pass
            else:
                print
                print self.controller.request.params[pars]
                print
                r = FirebaseActions.push("Canvas/",{"Notes":{"canvas-name":
                                 self.controller.request.params["personal"],
                                "creator":self.controller.session['youser'],
                                "date" : datetime.datetime.now(),"content":  self.controller.request.params[pars],
                                "id": pars
                                }})


        # self.controller.context['para'] = self.controller.session['pw']
        # r = FirebaseActions.push("Canvas/",{"Notes":{"canvas-name":
        #                          self.controller.request.params['personal'],
        #                         "creator":self.controller.session['youser'],
        #                         "date" : datetime.datetime.now(),"content":  self.controller.request.params['gener'],
        #                         "id": self.controller.request.params['txtAreaIdHide']
        #                         }})
        return self.controller.redirect(self.controller.uri(action='home',
          email=self.controller.session['youser'], password=self.controller.session['pw']))

        # return self.controller.redirect(self.controller.uri(action="login"))
        # return self.controller.redirect(self.controller.uri(action="home"))
        # r= FirebaseActions.push("Canvas/",{"Notes": {None}})




    def save_notes(self):
        kentot_whole = []
        kentot_ids = []
        kentot_dates = []
        r_canvas = FirebaseActions.get("Canvas/")
        for key, data in r_canvas.items():
            r_canvas2 = FirebaseActions.get("Canvas/"+key)
            for key2, data2 in r_canvas2.items():
                r_canvas3 = FirebaseActions.get("Canvas/"+key+"/Notes/date/isoformat")
                if r_canvas3 is None:
                    pass
                else:
                    kentot_dates.append(r_canvas3)

        kentot_dates.sort()
        self.controller.context['getmydate'] = (kentot_dates[-1])


        for key, data in r_canvas.items():
            r_canvas2 = FirebaseActions.get("Canvas/"+key)
            for key2, data2 in r_canvas2.items():
                kentot_whole.append([data2['id'],key])
                kentot_ids.append(data2['id'])

        for xid in kentot_ids:
            for ywhole in kentot_whole:
                if xid == ywhole[0]:
                    for parameter in self.controller.request.params:
                        if parameter == "save-notes" or parameter == "ok":
                            pass
                        else:
                            temp_parameter = parameter.split('*')
                            if temp_parameter[0] in kentot_ids:
                                print "update"
                                area_content1 = self.controller.request.params[parameter]
                                r = FirebaseActions.update("Canvas/"+temp_parameter[1]+"/Notes/",{"content": area_content1})
                            else:
                                print "push"
                                explodedcanvasname = str(self.controller.request.params['ok']).split("_")
                                area_content = self.controller.request.params[temp_parameter[0]]
                                r = FirebaseActions.push("Canvas/",{"Notes":{"canvas-name":
                                explodedcanvasname[0],
                                "creator":self.controller.session['youser'],
                                "date" : datetime.datetime.now(), "content": area_content, "id":temp_parameter[0]
                                }})
                # break the loop
                break
                print "break"
        return self.controller.redirect(self.controller.uri(action='home',
          email=self.controller.session['youser'], password=self.controller.session['pw']))



    def open_canvas(self):
        kentot = []
        r_canvas = FirebaseActions.get("Canvas/")
        exist = False
        for key, data in r_canvas.items():
            r_canvas2 = FirebaseActions.get("Canvas/"+key)
            for key2, data2 in r_canvas2.items():
                if data2['creator'] == str(self.controller.session['youser']):
                    kentot.append([data2['canvas-name'],data2['id']+'*'+key,data2['content']])
                    exist = True
                    break
        if exist:
            return kentot


    def home(self):
        mylst = []
        ctr = 0
        r_users = FirebaseActions.get("Users/")
        email = self.controller.request.params['email']
        password = self.controller.request.params['password']
        exist = False

        for key, data in r_users.items():
            if str(data['email']) == email and str(data['password']) == password:
                exist =True
                break
        if exist:
            #logging
            #logging.info("okay")
            self.controller.session['youser'] = email
            self.controller.session['pw'] = password
            #logging.info("************* %s" %self.controller.session['youser'])
            self.controller.context['youser'] = email  #original context
            if self.controller.open_canvas():  # determines if the user already have a canvas
                for xxx in self.controller.open_canvas():
                    if xxx[0] in mylst:
                        print "MERON na", xxx[0]
                    else:
                        print "WALA pa"
                        ctr = ctr + 1
                        mylst.append(xxx[0])
                print "mylist"
                print mylst
                self.controller.context['canvas_id'] = mylst       #original context
                self.controller.context['canvas_whole'] = self.controller.open_canvas()     #original context

                #this context was used in rendering template
                #must be a json format/dictionary
                context = {"youser": email,"canvas_id":mylst,"canvas_whole": self.controller.open_canvas()}
                return render_template("notefires/home.html",context=context)
            else:
                print "\n\n No canvas yet!"
                context = {"youser": email}
                return render_template("notefires/home.html",context=context)
        else:
            # logging.info("not okay")
            # action calls the function inside the class
            #if you want to call from another class
            #refer the line of code below
                # self.controller.redirect(self.controller.uri(controller='mails', action='index', user_email=email))
            #controller calls the controller, then call the function inside that controller
            return self.controller.redirect(self.controller.uri(action="login"))


    def ewaw(self):
        return "ewaw"
        # # params = {'email': "kenneth@h.k", 'password': "12345"}
        # # self.notefire.create(params)
        # # current_active_mapping = Notefire.semail("kenneth@h.k")
        # # print current_active_mapping
        # email = "jona@h.k"
        # password = "2"
        # f = FirebaseRest("Users/")
        # r = f.get()
        # json_encoded = json.dumps(r)
        # decoded_data = json.loads(json_encoded)
        # player  = None
        # exist = False

        # for key, data in decoded_data.items():
        #     if str(data['email']) == email and str(data['password']) == password:
        #         exist =True
        #         player = data
        #         break
        # if exist:
        #     print "okay"
        #     #------------UPDATE VIA BRUTE FORCE
        #     # FirebaseActions.remove('Users/', key)
        #     # r = f.push(json_stringify({"email": "email", "username": "username", "password": "password"}))
        #     #-----------UPDATE via FIREBASE_REST
        #     # ff = FirebaseRest("Users/"+key)
        #     # ff.update(json_stringify({"content":"tr"}))
        #     # self.controller.context['my_data'] = data['username']
        #     return
        # else:
        #     print "not okay"


    def get_latest_saved_date(self):
        kentot_dates = []
        r_canvas = FirebaseActions.get("Canvas/")
        for key, data in r_canvas.items():
            r_canvas2 = FirebaseActions.get("Canvas/"+key)
            for key2, data2 in r_canvas2.items():
                r_canvas3 = FirebaseActions.get("Canvas/"+key+"/Notes/date/isoformat")
                if r_canvas3 is None:
                    pass
                else:
                    kentot_dates.append(r_canvas3)
        kentot_dates.sort()
        self.controller.context['getmydate'] = (kentot_dates[-1])
        return self.controller.context['getmydate']





    def get_canvas_data(self):
        r_canvas = FirebaseActions.get("Canvas/")
        return r_canvas



    def get_users_data(self):
        r_canvas = FirebaseActions.get("Users/")
        return r_canvas
