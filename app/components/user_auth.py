from ferris import BasicModel, ndb

from google.appengine.api import users

def allow_access(controller, roles=None, allow_admin=True):
    if allow_admin:
        if controller.context.get('user_role') in ('SYSTEM_ADMINISTRATOR', 'ADMINISTRATOR', 'SUPERVISOR', 'SHIFT_LEADER'):
            return True

        if users.is_current_user_admin():
            return True

        if controller.context.get('is_admin'):
            return True

    context_user_role = controller.context.get('user_role', None)
    if context_user_role:
        if context_user_role in roles:
            return True
        else:
            return False

    # last check, OMDA users should not reach this point, only for authenticatd users via Auth
    if controller.context.get('auth_user'):
        return True

    return False
