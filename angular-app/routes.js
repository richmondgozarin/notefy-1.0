AppCore.config(
    function($routeProvider, $locationProvider) {
      $routeProvider.
       
        when('/notes/get-started', {
            templateUrl: 'ng/templates/notes/get-started.html',
            controller: 'getStartedController'
        }).
	when('/notes/create-note',{
	    templateUrl: 'ng/tempalates/notes/create-note.html',
	    controller: 'notesController'
	}).    
	when('/canvas/:canvas_id', {
	    templateUrl: 'ng/templates/notes/canvas.html',
            controller: 'canvasController'
            }).
        when('/logout', {
          controller : 'logoutController'
        }).
        otherwise({
            redirectTo: '/notes/get-started'
        });
	

  });
