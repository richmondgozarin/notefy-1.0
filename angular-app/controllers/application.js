
AppControllers.controller('applicationController', function($scope,$rootScope, $cookieStore, NoteRestLocal){
    $scope.links = {};
    $scope.account = {
      owner: 'rich'
    };

    if ($rootScope.globals.currentUser) {
      $scope.currentUser = $rootScope.globals.currentUser.username;
    }

    NoteRestLocal.canvasLists($scope.account, function(response) {
        console.log('canvasList');
        console.log(response);
        if(response.success) {
            $scope.links = response.data;
        } else {
            $scope.errors = response.message;
        }
        $scope.dataLoading = false;
    });

});
