AppControllers.controller('canvasController', function($scope,$rootScope, $cookieStore, NoteRestLocal){
    $scope.canvas = {
      title: '',
      id: '123123213',
      owner: 'rich'

    };
    $scope.account = {
      owner: 'rich'
    };


    $scope.canvasCreate = function(){
      console.log($scope.canvas.title);
      $scope.dataLoading = true;
      $scope.error = '';
      $scope.message = '';

      NoteRestLocal.canvasCreate($scope.canvas, function(response) {
          console.log(response);
          if(response.success) {
              $scope.message = 'Canvas created.';
              $('#canvasModal').modal('hide');

              NoteRestLocal.canvasLists($scope.account, function(response) {
                  console.log('updateing canvasList');
                  console.log(response);
                  if(response.success) {
                      $scope.links = response.data;
                  } else {
                      $scope.errors = response.message;
                  }
                  $scope.dataLoading = false;
              });

          } else {
              $scope.error = response.message;
          }
          $scope.dataLoading = false;
      });

      /*NoteRest.canvasCreate($scope.canvas)
        .success(function(data, status, headers, config){
          $scope.message = 'canvas created.';
          alert('Account Created.');

        }).error(function(data, status, headers, config){
            console.log(response.message);
            $scope.error = response.message;
            $scope.dataLoading = false;
      });*/
    };

  });
