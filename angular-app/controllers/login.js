
AppControllers.controller('loginController', function($scope,$rootScope, $location, AuthenticationService){
    /* Put your code here */
    $scope.account = {
      username: '',
      password: '',
      conpass: ''
    };


    AuthenticationService.ClearCredentials();

    $scope.login = function () {

        $scope.dataLoading = true;
        AuthenticationService.Login($scope.username, $scope.password, function(response) {
            if(response.success) {
                AuthenticationService.SetCredentials($scope.username, $scope.password);
                $location.path('/');
            } else {
                $scope.error = response.message;
                $scope.dataLoading = false;
            }
        });
    };

    $scope.register = function () {

        $scope.dataLoading = true;
        AuthenticationService.Register($scope.account, function(response) {
            if(response.success) {
                $location.path('/login');
            } else {
                $scope.error = response.message;
                $scope.dataLoading = false;
            }
        });
    };
});
