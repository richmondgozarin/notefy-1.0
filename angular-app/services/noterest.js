
AppServices.service('NoteRest', function($http, $cookieStore, $rootScope, $timeout) {
    return {
        accountLogin: function(params){
            return $http.get('https://notefy-rich-local.firebaseio.com/users.json');
               /* .then(function (res) {
                Session.create(params.email, params.password);
                return params;
                });*/
            //return $http.get('/api/accounts/login', {params:params});
        },

        accountCreate: function(params){
            return $http.post('/api/accounts/create', params);
        },
        accountList: function(params){
            return $http.get('/api/notefires/canvaslists', {params:params});
        },
        accountDetails: function(params){
            return $http.get('/api/accounts/account_details/'+params, {});
        },
        accountDelete: function(params){
            return $http.get('/api/accounts/delete/'+params, {params:params});
        },
        canvasCreate: function(params,callback){
            return $http.post('https://notefy-rich-local.firebaseio.com/users.json', params);
        },
        canvasList: function(params){
            return $http.get('/api/canvas', {params:params});
        },
        canvasDelete: function(params){
            return $http.get('/api/canvas/delete/'+params, {params:params});
        },

        notesCreate: function(params){
            return $http.post('/api/notes/create/', params);
        },
        notesList: function(params){
            return $http.get('/api/notes', {params:params});
        },
        notesDelete: function(params){
            return $http.get('/api/notes/delete/'+params, {params:params});
        },

        notesUpdate: function(params){
            return $http.post('api/notes/update/', params);
        },

        update: function(params){
            return this.save(params);
        },
        remove: function(params){
            return this.delete(params);
        }
    };
});


AppServices.service('NoteRestLocal', function($http, $cookieStore, $rootScope, $timeout) {
    var canvas_list = [
            {
                id: '123092392cscs2',
                title: 'My Qoutes',
                owner: 'rich'
            },
            {
                id: '1230wewrs2',
                title: 'My Notes',
                owner: 'rich'
            },
            {
                id: '22232332csApp2',
                title: 'My Things',
                owner: 'rich'
            },
            {
                id: '34222csdss2',
                title: 'My Collections',
                owner: 'rich'
            }
        ];

    return {
        canvasCreate:  function(params,callback){
            //return $http.post('https://notefy-rich-local.firebaseio.com/users.json', params);
            $timeout(function(){
                var response = { success:  params.title === 'test'};
                if(!response.success) {
                    response.message = 'Canvas title should be "test".';
                } else {
                    canvas_list.push(params);
                }
                callback(response);
            }, 1000);
        },
        canvasLists: function(params,callback){

            $timeout(function(){
                var response = { success:  params.owner === 'rich'};
                if(!response.success) {
                    response.message = 'Error while fetching data.';
                    response.data = [];
                }else
                {
                    response.message = 'data is fetch.';
                    response.data = canvas_list;
                }
                callback(response);
            }, 1000);
        }
    };

});
