'use strict';

  var app = angular.module('notefyApp', ['ngRoute', 'ngSanitize','new-notes']);

  app.config(
    function($routeProvider) {
      $routeProvider.
        when('/notes/get-started', {
          templateUrl: 'ng/templates/notes/get-started.html',
          controller: 'getStartedController'
        }).
        when('/notes/create-note', {
          templateUrl: 'ng/templates/notes/create-note.html',
          controller: 'notesController'
        }).
        when('/canvas/:canvas_id', {
          controller: 'canvasController'
        }).
        when('/logout', {
          controller : 'logoutController'
        }).
        otherwise({
            redirectTo: '/notes/get-started'
        });
  });

  app.controller('createNoteBtnController', function($scope,$compile){

    this.createNote = function(){
      // var node = '<div id="notes-div" class="well panel-default notes-div" contenteditable="True" my-draggable></div>';
      var node = '<create-note></create-note>';
      $(".container").append( $compile(node)($scope) );
    }

  });


  app.controller('getStartedController', function(){

  });

  app.controller('logoutController', function($window){
    $window.location.href = "http://localhost:8000/index.html";
    this.logout = function(){
      $window.location.href = "http://localhost:8000/index.html";
    }

  });

  app.controller('notesController', function(){

  });

  app.controller('canvasController', function($scope, $http, NoteRest){
    $scope.canvas = {};

    this.createCanvas = function(){
      NoteRest.canvasCreate($scope.canvas)
      .success(function(data, status, headers, config){
          if(status == 200){
              alert('New canvas created.');
          }
      }).error(function(data, status, headers, config){
          console.log('Adding canvas Failed');
          $scope.data.result = {};
    });
    }
  });



