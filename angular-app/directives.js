  AppDirectives.directive("modalCreateCanvas", function(){
    return {
     restrict: "E",
     templateUrl: "ng/templates/notes/canvas-modal.html"
    };
  });

  AppDirectives.directive("createNote", function(){
    return {
     restrict: "E",
     templateUrl: "ng/templates/notes/note.html"
    };
  });

  AppDirectives.directive('myDraggable', function($document) {
    return function(scope, element, attr) {
      var startX = 0, startY = 0, x = 0, y = 0;

      element.css({
       position: 'relative',
       border: '1px solid red',
       cursor: 'move'
      });

      element.on('dblclick', function(event) {
        event.preventDefault();
        startX = event.pageX - x;
        startY = event.pageY - y;
        $document.on('mousemove', mousemove);
        $document.on('mouseup', mouseup);
        console.log('dblclick');
        element.css({
          border: '1px solid blue',
          cursor: 'point',
          'z-index': 1,
          cursor: 'move'
        });
      });
      /*element.on('mousedown', function(event) {
        // Prevent default dragging of selected content
        event.preventDefault();
        element.focus();
        element.css({

          border: '1px solid blue',
          cursor: 'point',
          'z-index': 1
        });
      });*/

      function mousemove(event) {
        y = event.pageY - startY;
        x = event.pageX - startX;
        element.css({
          top: y + 'px',
          left:  x + 'px',
          border: '1px solid red',
          cursor: 'move',
          'z-index': 1
        });
      }

      function mouseup() {
        $document.off('mousemove', mousemove);
        $document.off('mouseup', mouseup);
        element.focus();
        element.css({
         border: '1px solid green',
         cursor: 'text'

        });
      }

    };

  });
