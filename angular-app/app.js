var AppControllers = angular.module('app.controllers', []);
var AppServices = angular.module('app.services', []);
var AppDirectives = angular.module('app.directives', []);
var AppFilters = angular.module('app.filters', []);
var AppConstants = angular.module('app.constants', []);
var AppCore = angular.module('app.core', ['ngRoute','ngAnimate','ngSanitize', 'ngCookies']);

var MainAppModule = angular.module('app', [
    'app.core',
    'app.services',
    'app.directives',
    'app.filters',
    'app.controllers',
    'app.constants'
]);

MainAppModule.run(
function ($rootScope, $location, $cookieStore, $http) {
    // keep user logged in after page refresh
    $rootScope.globals = $cookieStore.get('globals') || {};
    console.log($rootScope.globals.currentUser);

    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
    }

    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in
        if ($location.path() === '/register' && !$rootScope.globals.currentUser) {
            $location.path('/register');
        }else
        if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
            $location.path('/login');
        }
        else
        {
            $rootScope.globals ;
        }

    });
});
